package br.edu.up.canvas1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

public class Tabuleiro extends View {

  private Paint pincelAzul;
  private Paint pincelPreto;
  private Paint pincelVermelho;
  private Paint pincelVerde;

  public Tabuleiro(Context context) {
    super(context, null);
  }

  public Tabuleiro(Context context, AttributeSet attrs) {
    super(context, attrs);
    //setBackgroundColor(Color.LTGRAY);

    pincelAzul = new Paint();
    pincelAzul.setARGB(255, 0, 0, 255);

    pincelVerde = new Paint();
    pincelVerde.setARGB(255, 50, 100, 50);

    pincelPreto = new Paint();
    pincelPreto.setARGB(255, 0, 0, 0);

    pincelVermelho = new Paint();
    pincelVermelho.setARGB(255, 255, 0, 0);

    setFocusable(true);
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    int altura = canvas.getHeight();
    int largura = canvas.getWidth();
    int espessura = (int) (altura * 0.02);
    int quadro = (largura / 3) - espessura;

    pincelPreto.setStrokeWidth(espessura);
    pincelVermelho.setStrokeWidth((int) (espessura * 1.5));
    pincelVerde.setStrokeWidth((int) (espessura * 1.5));
    pincelVerde.setStyle(Paint.Style.STROKE);

    canvas.drawLine(0, quadro, largura, quadro, pincelPreto);
    canvas.drawLine(0, quadro * 2, largura, quadro * 2, pincelPreto);
    canvas.drawLine(quadro, 0, quadro, largura, pincelPreto);
    canvas.drawLine(quadro * 2, 0, quadro * 2, largura, pincelPreto);


    canvas.drawCircle(x + (quadro / 2), y + (quadro / 2), quadro / 2 - (espessura * 2), pincelVerde);

    Log.i("TAG", String.valueOf(x));

    canvas.drawLine(quadro + (0 + (espessura * 2)), 0 + (espessura * 2), quadro + (quadro - (espessura * 2)), quadro - (espessura * 2), pincelVermelho);
    canvas.drawLine(quadro + (quadro - (espessura * 2)), 0 + (espessura * 2), quadro + (0 + (espessura * 2)), quadro - (espessura * 2), pincelVermelho);

  }

  int x = 0;
  int y = 0;

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {

    if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
      x -= 10;
      invalidate();
      return true;
    } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
      x += 10;
      invalidate();
      return true;
    }

    return super.onKeyDown(keyCode, event);
  }
}